#include "ViewerApplication.hpp"
#include <iostream>
#include <numeric>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>
#include "utils/cameras.hpp"
#include <stb_image_write.h>
#include <tiny_gltf.h>


void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
		glfwSetWindowShouldClose(window, 1);
  	}
	if (key == GLFW_KEY_F && action == GLFW_RELEASE) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  	}
	if (key == GLFW_KEY_L && action == GLFW_RELEASE) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  	}
}


int ViewerApplication::run() {
	// Loader shaders
	const auto glslProgram =
		compileProgram({m_ShadersRootPath / m_vertexShader,
			m_ShadersRootPath / m_fragmentShader});

	// Get location of every needed uniform variables
	const auto modelViewProjMatrixLocation =
		glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
	const auto modelViewMatrixLocation =
		glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
	const auto normalMatrixLocation =
		glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");
	const auto uLightDirectionLocation =
		glGetUniformLocation(glslProgram.glId(), "uLightDirection");
  	const auto uLightIntensityLocation =
      	glGetUniformLocation(glslProgram.glId(), "uLightIntensity");
	// const auto uLightPositionLocation =
	// 	glGetUniformLocation(glslProgram.glId(), "uLightPosition");
  	// const auto uViewPositionLocation =
    //   	glGetUniformLocation(glslProgram.glId(), "uViewPosition");
	const auto uBaseColorTextureLocation =
		glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
	const auto uBaseColorFactorLocation =
		glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");
	const auto uMetallicRoughnessTextureLocation =
		glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");
	const auto uMetallicFactorLocation =
		glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
	const auto uRoughnessFactorLocation =
		glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");
	const auto uEmissiveTextureLocation =
		glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");
	const auto uEmissiveFactorLocation =
		glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");
	const auto uOcclusionTextureLocation =
		glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
	const auto uOcclusionStrengthLocation =
		glGetUniformLocation(glslProgram.glId(), "uOcclusionStrength");
	const auto uApplyOcclusionLocation =
		glGetUniformLocation(glslProgram.glId(), "uApplyOcclusion");
	const auto uNormalTextureLocation =
		glGetUniformLocation(glslProgram.glId(), "uNormalTexture");
	const auto uNormalScaleLocation =
		glGetUniformLocation(glslProgram.glId(), "uNormalScale");
	const auto uApplyNormalLocation =
		glGetUniformLocation(glslProgram.glId(), "uApplyNormal");
	const auto uApplyAdvNormalLocation =
		glGetUniformLocation(glslProgram.glId(), "uApplyAdvNormal");
	const auto uApplyUltraSurfaceLocation =
		glGetUniformLocation(glslProgram.glId(), "uApplyUltraSurface");
	const auto uApplyNormalColorsLocation =
		glGetUniformLocation(glslProgram.glId(), "uApplyNormalColors");

	tinygltf::Model model;
	// Loading the glTF file
	if(!loadGltfFile(model)) {
		return -1;
	}

	// Build projection matrix
	glm::vec3 bboxMin, bboxMax;
	computeSceneBounds(model, bboxMin, bboxMax);
	const auto diag = bboxMax - bboxMin;
	auto maxDistance = glm::length(diag);
	const auto projMatrix =
		glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
			0.001f * maxDistance, 1.5f * maxDistance);

	// Create camera, by default the TrackballCamera
	std::unique_ptr<CameraController> cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
	if (m_hasUserCamera) {
		cameraController->setCamera(m_userCamera);
	} else {
		const auto center = 0.5f * (bboxMax + bboxMin);
		const auto up = glm::vec3(0, 1, 0);
		const auto eye = diag.z > 0 ? center + diag : center + 2.f * glm::cross(diag, up);
		cameraController->setCamera(Camera{eye, center, up});
	}

	// Init light parameters
	glm::vec3 lightDirection(1, 1, 1);
	glm::vec3 lightIntensity(3, 3, 3);
	bool lightFromCamera = false; // light coming from camera
	bool applyOcclusion = true; // occlusion light
	bool applyNormalMap = true; // normal mapping
	bool applyAdvNormalMap = true; // normal mapping with tangent calculation
	bool applyUltraSurface = true; // details enhancer of surface when used with normal mapping
	bool normalMapColors = false; // change colors to be the normal map only (for debug)
	bool preventNormal = false; // if a model dosen't have a normal texture, prevent from calculating TBN

	// Creation of Texture Objects
	const auto textureObjs = createTextureObjects(model);

	// Create white texture for object with no base color texture
	GLuint whiteTexture = 0;
	glGenTextures(1, &whiteTexture);
	glBindTexture(GL_TEXTURE_2D, whiteTexture);
	float white[] = {1, 1, 1, 1};
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);

	// Creation of Buffer Objects
	const auto VBOs = createBufferObjects(model);

	// Creation of Vertex Array Objects
	std::vector<VaoRange> meshArrays;
	const auto VAOs = createVertexArrayObjects(model, VBOs, meshArrays);

	// Setup OpenGL state for rendering
	glEnable(GL_DEPTH_TEST);
	glslProgram.use();

	// Lambda function to bind material textures (bind uniform variables for textures)
	const auto bindMaterial = [&](const auto materialIndex) {
		// if the texture is defined at this index
		if(materialIndex >= 0) {
			const auto& material = model.materials[materialIndex];
			const auto& pbrMetallicRoughness = material.pbrMetallicRoughness;
			if(uBaseColorFactorLocation >= 0) {
				glUniform4f(uBaseColorFactorLocation,
					(float)pbrMetallicRoughness.baseColorFactor[0],
					(float)pbrMetallicRoughness.baseColorFactor[1],
					(float)pbrMetallicRoughness.baseColorFactor[2],
					(float)pbrMetallicRoughness.baseColorFactor[3]);
			}
			if(uBaseColorTextureLocation >= 0) {
				auto textureObject = whiteTexture;
				if(pbrMetallicRoughness.baseColorTexture.index >= 0) {
					const auto& texture = model.textures[pbrMetallicRoughness.baseColorTexture.index];
					if(texture.source >= 0) {
						textureObject = textureObjs[texture.source];
					}
				}
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, textureObject);
				glUniform1i(uBaseColorTextureLocation, 0);
			}
			if(uMetallicFactorLocation >= 0) {
				glUniform1f(uMetallicFactorLocation, (float)pbrMetallicRoughness.metallicFactor);
			}
			if(uRoughnessFactorLocation >= 0) {
				glUniform1f(uRoughnessFactorLocation, (float)pbrMetallicRoughness.roughnessFactor);
			}
			if(uMetallicRoughnessTextureLocation >= 0) {
				auto textureObject = 0u;
				if(pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) {
					const auto& texture = model.textures[pbrMetallicRoughness.metallicRoughnessTexture.index];
					if(texture.source >= 0) {
						textureObject = textureObjs[texture.source];
					}
				}
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, textureObject);
				glUniform1i(uMetallicRoughnessTextureLocation, 1);
			}
			if(uEmissiveFactorLocation >= 0) {
				glUniform3f(uEmissiveFactorLocation,
					(float)material.emissiveFactor[0],
					(float)material.emissiveFactor[1],
					(float)material.emissiveFactor[2]);
			}
			if(uEmissiveTextureLocation >= 0) {
				auto textureObject = 0u;
				if (material.emissiveTexture.index >= 0) {
					const auto &texture = model.textures[material.emissiveTexture.index];
					if (texture.source >= 0) {
						textureObject = textureObjs[texture.source];
					}
				}
				glActiveTexture(GL_TEXTURE2);
				glBindTexture(GL_TEXTURE_2D, textureObject);
				glUniform1i(uEmissiveTextureLocation, 2);
			}
			if(uOcclusionStrengthLocation >= 0) {
				glUniform1f(
					uOcclusionStrengthLocation, (float)material.occlusionTexture.strength);
			}
			if(uOcclusionTextureLocation >= 0) {
				auto textureObject = whiteTexture;
				if(material.occlusionTexture.index >= 0) {
					const auto &texture = model.textures[material.occlusionTexture.index];
					if(texture.source >= 0) {
						textureObject = textureObjs[texture.source];
					}
				}
				glActiveTexture(GL_TEXTURE3);
				glBindTexture(GL_TEXTURE_2D, textureObject);
				glUniform1i(uOcclusionTextureLocation, 3);
			}
			if(uNormalScaleLocation >= 0) {
				glUniform1f(uNormalScaleLocation, (float)material.normalTexture.scale);
			}
			if(uNormalTextureLocation >= 0) {
				auto textureObject = whiteTexture;
				if(material.normalTexture.index >= 0) {
					const auto& texture = model.textures[material.normalTexture.index];
					if(texture.source >= 0) {
						textureObject = textureObjs[texture.source];
					}
				}
				else preventNormal = true; // no normal texture
				glActiveTexture(GL_TEXTURE4);
				glBindTexture(GL_TEXTURE_2D, textureObject);
				glUniform1i(uNormalTextureLocation, 4);
			}
			if(preventNormal) { // reset all normal map flags if there is no normal map texture
				applyNormalMap = false;
				applyAdvNormalMap = false;
				applyUltraSurface = false;}
		}
		// if there is no defined texture, set variables to a default state
		else {
			if(uBaseColorFactorLocation >= 0) {
				glUniform4f(uBaseColorFactorLocation, 1, 1, 1, 1);
			}
			if(uBaseColorTextureLocation >= 0) {
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, whiteTexture);
				glUniform1i(uBaseColorTextureLocation, 0);
			}
			if(uMetallicFactorLocation >= 0) {
				glUniform1f(uMetallicFactorLocation, 1.f);
			}
			if(uRoughnessFactorLocation >= 0) {
				glUniform1f(uRoughnessFactorLocation, 1.f);
			}
			if(uMetallicRoughnessTextureLocation >= 0) {
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, 0);
				glUniform1i(uMetallicRoughnessTextureLocation, 1);
			}
			if(uEmissiveFactorLocation >= 0) {
				glUniform3f(uEmissiveFactorLocation, 0.f, 0.f, 0.f);
			}
			if(uEmissiveTextureLocation >= 0) {
				glActiveTexture(GL_TEXTURE2);
				glBindTexture(GL_TEXTURE_2D, 0);
				glUniform1i(uEmissiveTextureLocation, 2);
			}
			if(uOcclusionStrengthLocation >= 0) {
				glUniform1f(uOcclusionStrengthLocation, 0.f);
			}
			if(uOcclusionTextureLocation >= 0) {
				glActiveTexture(GL_TEXTURE3);
				glBindTexture(GL_TEXTURE_2D, 0);
				glUniform1i(uOcclusionTextureLocation, 3);
			}
			if(uNormalScaleLocation >= 0) {
				glUniform1f(uNormalScaleLocation, 0.f);
			}
			if(uNormalTextureLocation >= 0) {
				glActiveTexture(GL_TEXTURE4);
				glBindTexture(GL_TEXTURE_2D, whiteTexture);
				glUniform1i(uNormalTextureLocation, 4);
			}
		}
	};

	// Lambda function to draw the scene (called in the main loop)
	const auto drawScene = [&](const Camera &camera) {
		glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		const auto viewMatrix = camera.getViewMatrix();
		// const auto cameraPosition = camera.center();

		// bind uniform variables for light if defined
		if(uLightDirectionLocation >= 0) {
			if(lightFromCamera) {
				glUniform3fv(uLightDirectionLocation, 1, glm::value_ptr(glm::vec3(0,0,1)));
			}
			else {
				const auto lightDirectionInViewSpace = glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0.)));
				glUniform3fv(uLightDirectionLocation, 1, glm::value_ptr(lightDirectionInViewSpace));
			}
		}
		// if(uLightPositionLocation >= 0) {
		// 	if(lightFromCamera) {
		// 		glUniform3fv(uLightPositionLocation, 1, glm::value_ptr(cameraPosition));
		// 	}
		// 	else {
		// 		const auto lightDirectionInViewSpace = glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0.)));
		// 		glUniform3fv(uLightPositionLocation, 1, glm::value_ptr(lightDirectionInViewSpace * -1.0f));
		// 	}
		// }
		// if(uViewPositionLocation >= 0) {
		// 	glUniform3fv(uViewPositionLocation, 1, glm::value_ptr(cameraPosition));
		// }
		if(uLightIntensityLocation >= 0) {
			glUniform3fv(uLightIntensityLocation, 1, glm::value_ptr(lightIntensity));
		}
		if(uApplyOcclusionLocation >= 0) {
			glUniform1i(uApplyOcclusionLocation, applyOcclusion);
		}
		if(uApplyNormalLocation >= 0) {
			glUniform1i(uApplyNormalLocation, applyNormalMap);
		}
		if(uApplyAdvNormalLocation >= 0) {
			glUniform1i(uApplyAdvNormalLocation, applyAdvNormalMap);
		}
		if(uApplyUltraSurfaceLocation >= 0) {
			glUniform1i(uApplyUltraSurfaceLocation, applyUltraSurface);
		}
		if(uApplyNormalColorsLocation >= 0) {
			glUniform1i(uApplyNormalColorsLocation, normalMapColors);
		}

		// The recursive function that should draw a node
		// We use a std::function because a simple lambda cannot be recursive
		const std::function<void(int, const glm::mat4 &)> drawNode =
			[&](int nodeIdx, const glm::mat4 &parentMatrix) {
				const auto& node = model.nodes[nodeIdx];
				const glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);

				// draw a mesh (kind of a face, but with tree structure to allow resursive call)
				if(node.mesh >= 0) {
					const glm::mat4 MVMatrix = viewMatrix * modelMatrix;
					const glm::mat4 MVPMatrix = projMatrix * MVMatrix;
					const glm::mat4 normalMatrix = glm::transpose(glm::inverse(MVMatrix));
					glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(MVPMatrix));
					glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(MVMatrix));
					glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE, glm::value_ptr(normalMatrix));

					const auto& mesh = model.meshes[node.mesh];
					const auto& range = meshArrays[node.mesh];
					for(size_t pId = 0; pId < mesh.primitives.size(); pId++) {
						const auto vao = VAOs[range.begin + pId];
						const auto& primitive = mesh.primitives[pId];
						bindMaterial(primitive.material);
						glBindVertexArray(vao);
						if(primitive.indices >= 0) {
							const auto& accessor = model.accessors[primitive.indices];
							const auto& bufferView = model.bufferViews[accessor.bufferView];
							const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
							glDrawElements(primitive.mode, GLsizei(accessor.count), accessor.componentType, (const GLvoid*)byteOffset);
						}
						else {
							const auto accessorIdx = (*std::begin(primitive.attributes)).second;
							const auto& accessor = model.accessors[accessorIdx];
							glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
						}
					}
				}
				// draw every connected mesh to the actual mesh
				for(const auto childrenId : node.children) {
					drawNode(childrenId, modelMatrix);
				}
			};

		// Draw the scene referenced by gltf file
		if (model.defaultScene >= 0) {
			for(const auto nIdx : model.scenes[model.defaultScene].nodes) {
				drawNode(nIdx, glm::mat4(1));
			}
		}
	};

	// if RenderToImage option
	if(!m_OutputPath.empty()) {
		int nCompo = 3; // RGB == 3
		std::vector<unsigned char> pixels(m_nWindowWidth * m_nWindowHeight * nCompo);
		renderToImage(m_nWindowWidth, m_nWindowHeight, nCompo, pixels.data(), [&](){
			drawScene(cameraController->getCamera());
		});
		flipImageYAxis(m_nWindowWidth, m_nWindowHeight, nCompo, pixels.data());
		const std::string path = m_OutputPath.string();
		stbi_write_png(path.c_str(), m_nWindowWidth, m_nWindowHeight, nCompo, pixels.data(), 0);
		return 0;
	}

	// Loop until the user closes the window
	for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose(); ++iterationCount) {
		const auto seconds = glfwGetTime();
		const auto camera = cameraController->getCamera();
		drawScene(camera);

		// ImGUI code definition:
		imguiNewFrame();
		{
		ImGui::Begin("GUI");
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
			1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

		if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
			ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y, camera.eye().z);
			ImGui::Text("center: %.3f %.3f %.3f", camera.center().x, camera.center().y, camera.center().z);
			ImGui::Text("up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);
			ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y, camera.front().z);
			ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y, camera.left().z);

			if (ImGui::Button("CLI camera args to clipboard")) {
				std::stringstream ss;
				ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
					<< camera.eye().z << "," << camera.center().x << ","
					<< camera.center().y << "," << camera.center().z << ","
					<< camera.up().x << "," << camera.up().y << "," << camera.up().z;
				const auto str = ss.str();
				glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
			}

			static int cameraControllerType = 0;
			const auto cameraControllerTypeChanged =
				ImGui::RadioButton("Trackball", &cameraControllerType, 0) ||
				ImGui::RadioButton("First Person", &cameraControllerType, 1);
			if(cameraControllerTypeChanged) {
				const auto currentCamera = cameraController->getCamera();
				if(cameraControllerType == 0)
					cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
				else
					cameraController = std::make_unique<FirstPersonCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
				cameraController->setCamera(currentCamera);
			}
		}

		if(ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)) {
			static float lightTheta = 0.f;
        	static float lightPhi = 0.f;
			if(ImGui::SliderFloat("theta", &lightTheta, 0, glm::pi<float>()) ||
				ImGui::SliderFloat("phi", &lightPhi, 0, 2.f * glm::pi<float>())) {
					const auto sinPhi = glm::sin(lightPhi);
					const auto cosPhi = glm::cos(lightPhi);
					const auto sinTheta = glm::sin(lightTheta);
					const auto cosTheta = glm::cos(lightTheta);
					lightDirection = glm::vec3(sinTheta * cosPhi, cosTheta, sinTheta * sinPhi);
			}
			if(lightFromCamera) ImGui::Text("Position: %.3f %.3f %.3f", camera.eye().x, camera.eye().y, camera.eye().z);
			else ImGui::Text("Position: %.3f %.3f %.3f", lightDirection[0], lightDirection[1], lightDirection[2]);
			static glm::vec3 lightColor(1.f, 1.f, 1.f);
        	static float lightIntensityFactor = 3.f;
			if (ImGui::ColorEdit3("color", (float *)&lightColor) ||
				ImGui::InputFloat("intensity", &lightIntensityFactor)) {
					lightIntensity = lightColor * lightIntensityFactor;
			}
			ImGui::Checkbox("Light from camera", &lightFromCamera);
			ImGui::Checkbox("Apply occlusion", &applyOcclusion);
			if(!preventNormal) {
				ImGui::Checkbox("Normal Mapping", &applyNormalMap);
				if(applyNormalMap)ImGui::Checkbox("TBN Normal Mapping", &applyAdvNormalMap);
				else applyAdvNormalMap = false;
				if(applyAdvNormalMap) ImGui::Checkbox("Surface enhancer", &applyUltraSurface);
				else applyUltraSurface = false;
				if(applyUltraSurface) ImGui::Checkbox("Normal colors", &normalMapColors);
				else normalMapColors = false;
			}
			else ImGui::TextColored(ImVec4(255, 0, 0, 255), "This model dosen't have a Normal Map Texture.");
		}

		ImGui::End();
		}
		imguiRenderFrame();

		glfwPollEvents(); // Poll for and process events

		auto ellapsedTime = glfwGetTime() - seconds;
		auto guiHasFocus = ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
		if (!guiHasFocus) {
			cameraController->update(float(ellapsedTime));
		}
		m_GLFWHandle.swapBuffers(); // Swap front and back buffers
	}

	// clean VBOs and VAOs (IMPORTANT)
	glDeleteBuffers(VBOs.size(), VBOs.data());
    glDeleteVertexArrays(VAOs.size(), VAOs.data());

	return 0;
}


ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
	uint32_t height, const fs::path &gltfFile,
	const std::vector<float> &lookatArgs, const std::string &vertexShader,
	const std::string &fragmentShader, const fs::path &output) :
	m_nWindowWidth(width),
	m_nWindowHeight(height),
	m_AppPath{appPath},
	m_AppName{m_AppPath.stem().string()},
	m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
	m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
	m_gltfFilePath{gltfFile},
	m_OutputPath{output} {
		if (!lookatArgs.empty()) {
			m_hasUserCamera = true;
			m_userCamera =
				Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
					glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
					glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
		}

		if (!vertexShader.empty()) {
			m_vertexShader = vertexShader;
		}
		if (!fragmentShader.empty()) {
			m_fragmentShader = fragmentShader;
		}
 		// At exit, ImGUI will store its windows positions in this file
		ImGui::GetIO().IniFilename = m_ImGuiIniFilename.c_str();

		glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

		printGLVersion();
}


bool ViewerApplication::loadGltfFile(tinygltf::Model &model) {
	std::clog << "Loading file " << m_gltfFilePath << std::endl;
	tinygltf::TinyGLTF loader;
	std::string err; std::string warn;

	bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());

	if (!warn.empty()) std::cerr << warn << std::endl;
	if (!err.empty()) std::cerr << err << std::endl;
	if(!ret) {
		std::cerr << "Failed to parse glTF file" << std::endl;
		return false;
	}
	return true;
}


/*Define the default sampler to use in case there is no sampler defined for the asked texture*/
tinygltf::Sampler defaultSampler() {
	tinygltf::Sampler defaultSampler;
	defaultSampler.minFilter = GL_LINEAR;
	defaultSampler.magFilter = GL_LINEAR;
	defaultSampler.wrapS = GL_REPEAT;
	defaultSampler.wrapT = GL_REPEAT;
	defaultSampler.wrapR = GL_REPEAT;
	return defaultSampler;
}


std::vector<GLuint> ViewerApplication::createTextureObjects(const tinygltf::Model &model) const {
	std::vector<GLuint> texObj(model.textures.size(), 0);
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(GLsizei(texObj.size()), texObj.data());
	for(size_t i=0; i<model.textures.size(); i++) {
		const auto& texture = model.textures[i];
		assert(texture.source >= 0);
		const auto& image = model.images[texture.source];
		const auto& sampler = texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler();
		glBindTexture(GL_TEXTURE_2D, texObj[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, GL_RGBA, image.pixel_type, image.image.data());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);
		if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST || sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR || sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST || sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
			glGenerateMipmap(GL_TEXTURE_2D);
		}
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	return texObj;
}


std::vector<GLuint> ViewerApplication::createBufferObjects(const tinygltf::Model &model) {
	std::vector<GLuint> bufferObjects(model.buffers.size(), 0);

	glGenBuffers(GLsizei(model.buffers.size()), bufferObjects.data());
	for (size_t i=0; i<model.buffers.size(); i++) {
		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[i]);
		glBufferStorage(GL_ARRAY_BUFFER, model.buffers[i].data.size(), model.buffers[i].data.data(), 0);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return bufferObjects;
}


/** Compute manuel tangents if TANGENT is not found in the gltf model.
 * Create and bind a new VBO (for tangents) to the model's actual VAO. */
void createTangentArrayObject(const tinygltf::Model &model, const tinygltf::Primitive &primitive, GLuint attrib_idx){
	GLuint tangentBuffer;
	glGenBuffers(1, &tangentBuffer);
	std::vector<glm::vec3> tangents;

	const auto iterator = primitive.attributes.find("POSITION");
	if (iterator != end(primitive.attributes)) {
		const auto accessorIdx = (*iterator).second;
		const auto &accessor = model.accessors[accessorIdx];
		const auto &bufferView = model.bufferViews[accessor.bufferView];
		const auto bufferIdx = bufferView.buffer;
		const auto buffer = model.buffers[bufferIdx];
		const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
		const auto positionByteStride = bufferView.byteStride ? bufferView.byteStride : 3 * sizeof(float);
		const glm::vec3* vertices = reinterpret_cast<const glm::vec3*>(buffer.data.data() + byteOffset);
		const auto nbVertice = accessor.count;

		const auto uv_iterator = primitive.attributes.find("TEXCOORD_0");
		if (uv_iterator != end(primitive.attributes)){
			const auto uv_accessorIdx = (*uv_iterator).second;
			const auto &uv_accessor = model.accessors[uv_accessorIdx];
			const auto &uv_bufferView = model.bufferViews[uv_accessor.bufferView];
			const auto uv_bufferIdx = uv_bufferView.buffer;
			const auto uv_buffer = model.buffers[uv_bufferIdx];
			const auto uv_byteOffset = uv_accessor.byteOffset + uv_bufferView.byteOffset;
			const auto uv_positionByteStride = uv_bufferView.byteStride ? uv_bufferView.byteStride : 2 * sizeof(float);
			// const glm::vec2* uvs = reinterpret_cast<const glm::vec2*>(buffer.data.data() + uv_byteOffset);

			std::vector<glm::vec3> positions;
			std::vector<glm::vec2> uvs;

			if (primitive.indices >= 0) {
				const auto &indexAccessor = model.accessors[primitive.indices];
				const auto &indexBufferView =
					model.bufferViews[indexAccessor.bufferView];
				const auto indexByteOffset =
					indexAccessor.byteOffset + indexBufferView.byteOffset;
				const auto &indexBuffer = model.buffers[indexBufferView.buffer];
				auto indexByteStride = indexBufferView.byteStride;

				switch (indexAccessor.componentType) {
					default: std::cerr << "Primitive index accessor with bad componentType "
							<< indexAccessor.componentType << ", skipping it." << std::endl;
							// continue;
					case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
						indexByteStride = indexByteStride ? indexByteStride : sizeof(uint8_t); break;
					case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
						indexByteStride = indexByteStride ? indexByteStride : sizeof(uint16_t); break;
					case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
						indexByteStride = indexByteStride ? indexByteStride : sizeof(uint32_t); break;
				}

				for (size_t i = 0; i < indexAccessor.count; ++i) {
					uint32_t index = 0;
					switch (indexAccessor.componentType) {
						case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
							index = *((const uint8_t *)&indexBuffer.data[indexByteOffset + indexByteStride * i]);
							break;
						case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
							index = *((const uint16_t *)&indexBuffer.data[indexByteOffset + indexByteStride * i]);
							break;
						case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
							index = *((const uint32_t *)&indexBuffer.data[indexByteOffset + indexByteStride * i]);
							break;
					}

					const auto &localPosition = *((const glm::vec3 *)&buffer.data[byteOffset + positionByteStride * index]);
					positions.push_back(localPosition);
					const auto &localUV = *((const glm::vec2 *)&uv_buffer.data[uv_byteOffset + uv_positionByteStride * index]);
					uvs.push_back(localUV);
				}
			} else {
				for (size_t i = 0; i < accessor.count; ++i) {
					const auto &localPosition = *((const glm::vec3 *)&buffer.data[byteOffset + positionByteStride * i]);
					positions.push_back(localPosition);
					const auto &localUV = *((const glm::vec2 *)&uv_buffer.data[uv_byteOffset + uv_positionByteStride * i]);
					uvs.push_back(localUV);
				}
			}
		
			for (int i = 0; i < positions.size(); i += 3){
				glm::vec3 v1 = positions[i];
				glm::vec3 v2 = positions[i + 1];
				glm::vec3 v3 = positions[i + 2];

				glm::vec2 uv1 = uvs[i];
				glm::vec2 uv2 = uvs[i + 1];
				glm::vec2 uv3 = uvs[i + 2];

				glm::vec3 edge1 = v2 - v1;
				glm::vec3 edge2 = v3 - v1;
				glm::vec2 deltaUV1 = uv2 - uv1;
				glm::vec2 deltaUV2 = uv3 - uv1;  

				float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);
				float x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
				float y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
				float z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);

				tangents.push_back(glm::vec3(x, y, z));
			}
		}
		glEnableVertexAttribArray(attrib_idx);
		glBindBuffer(GL_ARRAY_BUFFER, tangentBuffer);
		glBufferData(GL_ARRAY_BUFFER, tangents.size() * sizeof(glm::vec3), tangents.data(), GL_STATIC_DRAW);
		glVertexAttribPointer(attrib_idx, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), nullptr);
	}
}


std::vector<GLuint> ViewerApplication::createVertexArrayObjects(const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects, std::vector<VaoRange> &meshIndexToVaoRange) {
	std::vector<GLuint> vertexArrayObjects;
	meshIndexToVaoRange.resize(model.meshes.size());

	std::vector<GLuint> VERTEX_ATTRIB_IDX = {0, 1, 2, 3}; // POSITION, NORMAL, TEXCOORD_0, TANGENT
	std::vector<std::string> VERTEX_ATTRIB_NAMES = {"POSITION", "NORMAL", "TEXCOORD_0", "TANGENT"};

	for (size_t i = 0; i < model.meshes.size(); ++i) {
		const auto &mesh = model.meshes[i];
		auto &vaoRange = meshIndexToVaoRange[i];
		vaoRange.begin = GLsizei(vertexArrayObjects.size());
		vaoRange.count = GLsizei(mesh.primitives.size());

		vertexArrayObjects.resize(vertexArrayObjects.size() + mesh.primitives.size());
		glGenVertexArrays(vaoRange.count, &vertexArrayObjects[vaoRange.begin]);

		for (size_t pIdx = 0; pIdx < mesh.primitives.size(); ++pIdx) {
			const auto vao = vertexArrayObjects[vaoRange.begin + pIdx];
			const auto &primitive = mesh.primitives[pIdx];
			glBindVertexArray(vao);

			for(int i=0; i<VERTEX_ATTRIB_IDX.size(); i++) {
				const GLuint attrib_idx = VERTEX_ATTRIB_IDX[i];
				std::string attrib_name = VERTEX_ATTRIB_NAMES[i];

				const auto iterator = primitive.attributes.find(attrib_name);
				if (iterator != end(primitive.attributes)) {
					const auto accessorIdx = (*iterator).second;
					const auto &accessor = model.accessors[accessorIdx];
					const auto &bufferView = model.bufferViews[accessor.bufferView];
					const auto bufferIdx = bufferView.buffer;

					glEnableVertexAttribArray(attrib_idx);
					assert(GL_ARRAY_BUFFER == bufferView.target);

					glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

					const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
					glVertexAttribPointer(attrib_idx, accessor.type,
						accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
						(const GLvoid *)byteOffset);
				}
				else if (attrib_name.compare("TANGENT") == 0) { // TANGENT not found, compute manual tangents
					createTangentArrayObject(model, primitive, attrib_idx);
				}
			}
			// if Index array if defined
			if (primitive.indices >= 0) {
				const auto accessorIdx = primitive.indices;
				const auto &accessor = model.accessors[accessorIdx];
				const auto &bufferView = model.bufferViews[accessor.bufferView];
				const auto bufferIdx = bufferView.buffer;
				assert(GL_ELEMENT_ARRAY_BUFFER == bufferView.target);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[bufferIdx]);
			}
		}
	}
	glBindVertexArray(0);
	std::clog << "Number of VAOs: " << vertexArrayObjects.size() << std::endl;
	return vertexArrayObjects;
}
