#version 330

/*
same thing as forward shader but with normal mapping/tangent implementation
the tangent/TBN calculation is optional and will happen if uApplyAdvNormal is true

this vs shader should be used with pbr_light_normalMap.fs
*/

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTexCoords;
layout(location = 3) in vec3 aTangent;

out vec3 vViewSpacePosition;
out vec3 vViewSpaceNormal;
out vec2 vTexCoords;
out mat3 TBN;
// out vec3 vTangentLightPosition;
// out vec3 vTangentViewPosition;
// out vec3 vTangentFragPosition;

uniform mat4 uModelViewProjMatrix;
uniform mat4 uModelViewMatrix;
uniform mat4 uNormalMatrix;
// uniform vec3 uLightPosition;
// uniform vec3 uViewPosition;

uniform int uApplyAdvNormal; // to know if we want TBN calculation or not
flat out int vApplyAdvNormal; // pass the flag to the fragment shader


void main() {
    vApplyAdvNormal = 0;
    vViewSpacePosition = vec3(uModelViewMatrix * vec4(aPosition, 1));
    vViewSpaceNormal = normalize(vec3(uNormalMatrix * vec4(aNormal, 0)));
    vTexCoords = aTexCoords;
    // IF normal mapping TBN calculation
    // from https://learnopengl.com/Advanced-Lighting/Normal-Mapping
    if(uApplyAdvNormal == 1) {
        vApplyAdvNormal = 1;
        vec3 T = normalize(vec3(uNormalMatrix * vec4(aTangent, 0.0)));
        vec3 N = normalize(vec3(uNormalMatrix * vec4(aNormal, 0.0)));
        T = normalize(T - dot(T, N) * N); // re-orthogonalize T with respect to N
        vec3 B = normalize(cross(N, T)); // retrieve perpendicular vector B
        TBN = mat3(T, B, N);
        // mat3 TBN = transpose(mat3(T, B, N));
        // vTangentLightPosition = TBN * uLightPosition;
        // vTangentViewPosition = TBN * uViewPosition;
        // vTangentFragPosition = TBN * vViewSpacePosition;
    }
    gl_Position =  uModelViewProjMatrix * vec4(aPosition, 1);
}